package com.night.imagetext

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore.Images
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.googlecode.tesseract.android.TessBaseAPI
import com.night.opencvdemo.databinding.ActivityImageTextBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

/**
 * @author 刘守康
 * 创建日期：2023/8/25
 * 描述：IdCardActivity
 */
class IdCardActivity :AppCompatActivity() {
    private lateinit var viewBinding: ActivityImageTextBinding


    private var fullImage: Bitmap? = null
    private var resultBitmap:Bitmap? =null
    private lateinit var tessBaseAPI: TessBaseAPI
    @SuppressLint("IntentReset")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityImageTextBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        viewBinding.btnIdCard.setOnClickListener {
            val bitmap =
                fullImage?.let { it1 -> getIdNumber(it1, Bitmap.Config.ARGB_8888)
            } ?: run {
                Toast.makeText(this, "为识别到身份证", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            fullImage?.recycle()
            resultBitmap = bitmap
            viewBinding.ivContent.setImageBitmap(bitmap)

        }
        viewBinding.btnSelectImg.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_PICK,
                Images.Media.EXTERNAL_CONTENT_URI
            )
            intent.type = "image/*"
            //使用选取器并自定义标题
            startActivityForResult(Intent.createChooser(intent, "选择待识别图片"), 100)
        }
        runBlocking {
            tessBaseAPI = initTessTwo()
        }
        viewBinding.btnGetText.setOnClickListener {
            tessBaseAPI.setImage(resultBitmap)
            val text = tessBaseAPI.utF8Text;
            if(!TextUtils.isEmpty(text)){
                viewBinding.tvIdNumber.text = tessBaseAPI.utF8Text
            }else{
                Toast.makeText(this, "这只是一张秃瓢换一张试试", Toast.LENGTH_SHORT).show()
            }
            tessBaseAPI.clear()
        }
    }

    //身份证
    private val languageCn ="cn"
    //数字
    private val languageEnm ="enm"

    @SuppressLint("SdCardPath")
    private suspend fun initTessTwo(): TessBaseAPI {
        //异步操作
        return coroutineScope {
            val tes = TessBaseAPI()
            //加载 assets文件夹下的模型数据
            val inputStream = assets.open("$languageCn.traineddata")
            //将数据包存入sdcard
            val dataFile = File("/sdcard/tess/tessdata/"+"${languageCn}.traineddata")
            withContext(Dispatchers.IO) {
                if (!dataFile.exists()) {
                    dataFile.parentFile?.mkdirs()
                    val fos = FileOutputStream(dataFile)
                    val buffer = ByteArray(2048)
                    var len: Int
                    while (inputStream.read(buffer).also { len = it } != -1) {
                        fos.write(buffer, 0, len)
                    }
                    fos.close()
                }
                inputStream.close()
            }
            tes.init("/sdcard/tess", languageCn)
            tes
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && data != null) {
            getResult(data.data)
        }
    }

    private fun getResult(uri: Uri?) {
        var imagePath: String? = null
        if (null != uri) {
            if ("file" == uri.scheme) {
                imagePath = uri.path
            } else if ("content" == uri.scheme) {
                val filePathColumns = arrayOf(Images.Media.DATA)
                val c = contentResolver.query(uri, filePathColumns, null, null, null)
                if (null != c) {
                    if (c.moveToFirst()) {
                        val columnIndex = c.getColumnIndex(filePathColumns[0])
                        imagePath = c.getString(columnIndex)
                    }
                    c.close()
                }
            }
        }
        if (!TextUtils.isEmpty(imagePath)) {
            if (fullImage != null) {
                fullImage?.recycle()
            }
            fullImage = toBitmap(imagePath)
            viewBinding.ivContent.setImageBitmap(fullImage)
        }
    }
    private fun toBitmap(pathName: String?): Bitmap? {
        if (TextUtils.isEmpty(pathName)) return null
        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeFile(pathName, o)
        var width_tmp = o.outWidth
        var height_tmp = o.outHeight
        var scale = 1
        while (true) {
            if (width_tmp <= 640 && height_tmp <= 480) {
                break
            }
            width_tmp /= 2
            height_tmp /= 2
            scale *= 2
        }
        val opts = BitmapFactory.Options()
        opts.inSampleSize = scale
        opts.outHeight = height_tmp
        opts.outWidth = width_tmp
        return BitmapFactory.decodeFile(pathName, opts)
    }

    companion object{
         init {
             System.loadLibrary("opencv")
         }
     }


     private external fun imageToText()

    /**
     * 获取身份证号码
     */
    private external fun getIdNumber(src:Bitmap,config:Bitmap.Config):Bitmap
}