package com.night

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.googlecode.tesseract.android.TessBaseAPI
import com.night.cameracalibration.CameraCalibrationActivity
import com.night.colorblobdetect.ColorBlobDetectionActivity
import com.night.imagemanipulations.ImageManipulationsActivity
import com.night.imagetext.IdCardActivity
import com.night.opencvdemo.R
import com.night.puzzle15.Puzzle15Activity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.opencv.android.OpenCVLoader
import java.io.File
import java.io.FileOutputStream


class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initLoadOpenCV()
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
                //当用户第一次申请拒绝时，再次申请该权限调用
                Toast.makeText(this, "读写sdcard的权限", Toast.LENGTH_SHORT).show()
            }
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.MANAGE_EXTERNAL_STORAGE
                ),
                100
            )

            // 高版本Android SDK时使用如下代码
            if (!Environment.isExternalStorageManager()) {
                val intent = Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION)
                startActivity(intent)
                return
            }
        } else {
            Toast.makeText(this, "已有读写sdcard的权限", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initLoadOpenCV() {
        val success = OpenCVLoader.initDebug()
        if (success) {
            Log.d("init", "initLoadOpenCV: openCV load success")
        } else {
            Log.e("init", "initLoadOpenCV: openCV load failed")
        }
    }

    fun cvtColorActivity(view: View) {

        startActivity(Intent(this, CvtColorActivity::class.java))

    }

    fun imageManipulations(view: View) {
        startActivity(Intent(this, ImageManipulationsActivity::class.java))

    }

    fun puzzle(view: View) {
        startActivity(Intent(this, Puzzle15Activity::class.java))
    }
    fun colorBlobDetection(view: View) {
        startActivity(Intent(this, ColorBlobDetectionActivity::class.java))
    }
    fun cameraCalibration(view: View) {
        startActivity(Intent(this, CameraCalibrationActivity::class.java))
    }

    //身份证识别
    fun manualCut(view: View) {
        startActivity(Intent(this, IdCardActivity::class.java))
    }

}