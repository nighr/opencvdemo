package com.night

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.night.opencvdemo.R
import com.night.opencvdemo.databinding.ActivityCvtColorBinding
import org.opencv.android.Utils
import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc

/**
 * @author 刘守康
 * 创建日期：2023/8/22
 * 描述：CvtColorActivity
 */
class CvtColorActivity : AppCompatActivity() {


    lateinit var bing: ActivityCvtColorBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bing = ActivityCvtColorBinding.inflate(layoutInflater)

        setContentView(bing.root)

    }


    /**
     *灰度图片如何处理
     * 图片ARGB RGBA（openCV）
     * 取单色
     */
    fun cvtColor(view: View) {
        //获取bitmap
        val bitmap  =BitmapFactory.decodeResource(resources, R.drawable.img)
        val src = Mat()
        val dst = Mat()
        Utils.bitmapToMat(bitmap,src)
        Imgproc.cvtColor(src,dst,Imgproc.COLOR_RGB2GRAY)
        Utils.matToBitmap(dst,bitmap)
        bing.image.setImageBitmap(bitmap)
        src.release()
        dst.release()
    }


    /**
     * 添加背景
     * ps 功能
     */
    fun addMat(view:View){
        val matBg = Utils.loadResource(this,R.drawable.bg_color)
        val matSrc = Utils.loadResource(this,R.drawable.img)
        val dst = Mat()
        Core.bitwise_and(matBg,matSrc,dst)
        val bitmap = Bitmap.createBitmap(dst.width(),dst.height(),Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(dst,bitmap)
        bing.image.setImageBitmap(bitmap)
        matBg.release()
        matSrc.release()
        dst.release()
    }

}