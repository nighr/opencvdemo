package com.night

import android.app.Application
import androidx.core.app.AppLaunchChecker
import com.googlecode.tesseract.android.TessBaseAPI

/**
 * @author 刘守康
 * 创建日期：2023/9/1
 * 描述：MyApplication
 */
class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
    }

}