//
// Created by liushoukang on 2023/8/30.
//
#include <jni.h>
#include <android/log.h>
#include "jni/include/opencv2/core.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv2/imgproc.hpp"
//
// Created by liushoukang on 2023/8/25.
//

#define CARD_SIZE Size(640,400)
#define LOG_TAG "C_TAG"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__);
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

using namespace cv;

extern "C" JNIEXPORT void JNICALL Java_org_opencv_android_Utils_nBitmapToMat2
        (JNIEnv *env, jclass, jobject bitmap, jlong m_addr, jboolean needUnPremultiplyAlpha);
extern "C"
JNIEXPORT void JNICALL
Java_org_opencv_android_Utils_nMatToBitmap2(JNIEnv *env, jclass clazz, jlong m_addr, jobject b,
                                            jboolean premultiply_alpha);
jobject createBitmap(JNIEnv *pEnv, Mat mat, jobject config) {

    int matWidth = mat.cols;
    int matHeight = mat.rows;
    int matPix = matWidth * matHeight;
    jclass bitmapClazz = pEnv->FindClass("android/graphics/Bitmap");
    jmethodID createdBitmapMethod = pEnv->GetStaticMethodID(bitmapClazz, "createBitmap", "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
    jobject objBitmap = pEnv->CallStaticObjectMethod(bitmapClazz, createdBitmapMethod,
                                                     matWidth,
                                                     matHeight, config);
    Java_org_opencv_android_Utils_nMatToBitmap2(pEnv,nullptr,jlong (&mat),objBitmap, false);
    return objBitmap;
}

using namespace std;

extern "C"
JNIEXPORT void JNICALL
Java_com_night_imagetext_IdCardActivity_imageToText(JNIEnv *env, jobject thiz) {
    LOGD("这是打印的一句话      ")

    Mat src_img;
    Mat dst_img;
    int a = getVersionRevision();

    src_img.release();
    dst_img.release();
    LOGD("这是打印的一句话   版本是个%s    ", CV_VERSION)

}
extern "C"
JNIEXPORT jobject JNICALL
Java_com_night_imagetext_IdCardActivity_getIdNumber(JNIEnv *env, jobject thiz, jobject src,
                                                    jobject config) {
    //原文件
    Mat src_img;
    //目标
    Mat dst_img;

    jclass clazz = env->GetObjectClass(thiz);

    //将bitmap转换成mat
    Java_org_opencv_android_Utils_nBitmapToMat2(env, clazz, src, jlong(&src_img), false);
    Mat dst;
    //无损压缩 640*400
    resize(src_img, src_img, CARD_SIZE);
    //灰值化
    cvtColor(src_img, dst, COLOR_BGR2GRAY);
    //二值化
    threshold(dst, dst, 100, 255, THRESH_BINARY);
    // 膨胀
    Mat erodeElement = getStructuringElement(MORPH_RECT, Size(20, 10));
    erode(dst, dst, erodeElement);
    // 轮廓检测
    vector<vector<Point>> contours;
    vector<Rect> rects;

    findContours(dst, contours, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));
    for (auto &contour: contours) {
        Rect rect = boundingRect(contour);
        rectangle(dst, rect, Scalar(0, 255));
        // 筛选轮廓图片
        if (rect.width > rect.height * 9) {
            rects.push_back(rect);
            rectangle(dst, rect, Scalar(0, 0, 255));
        }
    }
    if(rects.empty()){
        LOGD("你这个照片不行 换一张试试 ====")
        return nullptr;
    }
    if (rects.size() == 1) {
        dst_img = src_img(rects.at(0));
    } else {
        Rect rectTmp = rects.at(0);
        // 遍历查找Y最大的轮廓
        for (auto rect: rects) {
            if (rect.tl().y > rectTmp.tl().y) {
                rectTmp = rect;
            }
        }
        rectangle(dst, rectTmp, Scalar(255, 255, 0));
        dst_img = src_img(rectTmp);
    }
    jobject bitmap = createBitmap(env, dst_img, config);
    src_img.release();
    dst_img.release();
    dst.release();
    return bitmap;
}

